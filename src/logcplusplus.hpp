/** @brief include this file to use the logcplusplus library
 */

#include <list>
#include <queue>
#include <iostream>
#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#ifndef _LOGCPLUSPLUS_HPP_
#define _LOGCPLUSPLUS_HPP_
namespace LgCpp{

class Sink;
/** @brief source of logged message
 *
 *  This is the base loggin class. It is used to log any message and they can be as many as needed. Basically, one can implement this object in any other custom object to log specific message. Loggers can have a name which will be displayed in the final message. Those name don't have to be unique.
 */
class Logger
{
friend class Sink;
public:
    Logger(std::string inName);
    ~Logger() {}
    
    /// defines the severity of a message
    enum Severity
    {
        logDEBUG = 0,
        logINFO,
        logWARNING,
        logERROR,
        logCRITICAL,
        logLastItem
    };
    
    int logIt(const std::string &inLog, Severity inSeverity);///< logs an incoming data in the registered sinks
    int logIt(const std::ostringstream &inLog, Severity inSeverity);
    int logIt(const char* inLog, Severity inSeverity);
    static int setSeverityDescriptor( const std::map<Severity, std::string> & inSeverityDescriptor);
 
    const std::string& getLoggerName() const {return _loggerName;} ///< returns the logger name
    const std::string& getLastLoggedMsg() const {return _lastLoggedMsg;} ///< returns the last logged message
    Severity getLastLoggedSev() const {return _lastLoggedSev;} ///< returns the serverity of the last logged message
    boost::posix_time::ptime getLogTime() const {return _lastLogTime;} ///< returns the date-time of the last logged message

private:
    std::string _loggerName;
    std::string _lastLoggedMsg;
    Severity _lastLoggedSev;
    boost::posix_time::ptime _lastLogTime; 
    
    static std::map<Severity, std::string> _severityDescriptor; ///< severity string descriptor
    static std::list<Sink*> _listSink;  ///< lists of the registered sinks
    static boost::mutex _mutexListSink; ///< protect the list of sinks
    
protected:
    static int regSink( Sink &inSink ); ///< register a sink, called by the Sink Constructor
    static int remSink( Sink &inSink ); ///< unregister a sink, called by the Sink Destructor

};

/** @brief destination of logged message
 *
 *  This is the base sink class. It is the destination of the messages. For each sink a threshold severity can be defined above which a message will be logged.
 *
 *  @todo implement a call back solution to select the logged message
 */
class Sink
{
friend class Logger;
public:
    Sink(std::ostream &inOstream);
    Sink(std::string &inFileName);
    ~Sink();

    /** sets the severity above which a message will be logged
     *  @param[in] inSeverity: threshold severity. Message with a lower severity will be ignored.
     */
    void setLogSeverity( Logger::Severity inSeverity )
    {
        _logSeverity = inSeverity;
    }
       
private:
    int logIt( const Logger *inLogger );
    
    bool _deleteStreamAtDestroy;

    Logger::Severity _logSeverity;
    std::ostream* _ptrTargetStream;
};
}

#endif
