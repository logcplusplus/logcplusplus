#include <fstream>
#include "logcplusplus.hpp"

namespace LgCpp{

std::map<Logger::Severity, std::string> Logger::_severityDescriptor;
std::list<Sink*> Logger::_listSink;
boost::mutex Logger::_mutexListSink;

/** Logger constructor
 *  @param[in] inName: name of the logger as it will be logged in the sinks.
 */
Logger::Logger(std::string inName): _loggerName(inName)
{
    if(_severityDescriptor.size() == 0)
    {
        _severityDescriptor[logDEBUG] = "DEBUG";
        _severityDescriptor[logINFO] = "INFO";
        _severityDescriptor[logWARNING] = "WARNING";
        _severityDescriptor[logERROR] = "ERROR";
        _severityDescriptor[logCRITICAL] = "CRITICAL";
    }
}

/** sets the human readable description of the severity values
 *  @param[in] inSeverityDescriptor : new severity description map to use
 */
int Logger::setSeverityDescriptor( const std::map<Severity, std::string> & inSeverityDescriptor )
{
    _severityDescriptor = inSeverityDescriptor;
    return 0;
}

/** @overload int LgCpp::Logger::logIt(const std::string &inLog, Severity inSeverity)
*/
int Logger::logIt(const char* inLog, Severity inSeverity)
{
    std::string message(inLog);
    return logIt(message, inSeverity);
}

/** @overload int LgCpp::Logger::logIt(const std::string &inLog, Severity inSeverity)
*/
int Logger::logIt(const std::ostringstream &inLog, Severity inSeverity)
{
    std::string message(inLog.str());
    return logIt(message, inSeverity);
}
          
/** This is the mostly used function since it will be used to log any message.
 *  @param[in] inLog: message to log
 *  @param[in] inSeverity: severity of the message (error, warning, info...)
 *  @return always 0.
*/
int Logger::logIt(const std::string &inLog, Severity inSeverity)
{
    _lastLoggedMsg = inLog;
    _lastLoggedSev = inSeverity;
    _lastLogTime = boost::posix_time::microsec_clock::universal_time();
    
    _mutexListSink.lock();
        std::list<Sink*>::iterator itListSink;
        for( itListSink = _listSink.begin(); itListSink != _listSink.end(); ++itListSink )
        {
            (*itListSink)->logIt( this );
        }
    _mutexListSink.unlock();
    return 0;
}

/** registers a new sink. If this function is called while the sink is already registered, nothing happens.
 *  @param[in] inSink: sink to register
 */
int Logger::regSink( Sink &inSink )
{
    _mutexListSink.lock();
        std::list<Sink*>::iterator itListSink;
        bool found = false;
        for( itListSink = _listSink.begin(); ( itListSink != _listSink.end() ) && ( found == false ); ++itListSink )
        {
           found = ( *itListSink == &inSink );
        }
        
        if(!found)
            _listSink.push_back(&inSink);
    _mutexListSink.unlock();
    return 0;
}

/** unregisters a sink. If this function is called while the sink is already unregistered, nothing happens.
 *  @param[in] inSink: sink to unregister
 */
int Logger::remSink( Sink &inSink )
{
    _mutexListSink.lock();
        std::list<Sink*>::iterator itListSink;
        std::list<Sink*>::iterator itTargetListSink;
        bool found = false;
        for( itListSink = _listSink.begin(); ( itListSink != _listSink.end() ) && ( found == false ); ++itListSink )
        {
            if( *itListSink == &inSink )
            {
                found = true;
                itTargetListSink = itListSink;
            }
        }
        
        if(found)
        {
            _listSink.erase(itTargetListSink);
        }
    _mutexListSink.unlock();
    return 0;    
}

/** creates a sink from an input stream reference. The reference must be valid as long as the sink exists.
 *  @note Destroying the sink will not destroy the stream.
 *  @param inStream: output stream to use as sink.
 */
Sink::Sink( std::ostream &inStream ): _ptrTargetStream(NULL), _deleteStreamAtDestroy(false),
    _logSeverity(Logger::Logger::logERROR)
{
    _ptrTargetStream = &inStream;
    Logger::regSink(*this);
}

/** creates a sink from an input file path.
 *  @param inFileName: output file.
 */
Sink::Sink( std::string &inFileName ): _ptrTargetStream(NULL), _deleteStreamAtDestroy(true),
    _logSeverity(Logger::Logger::logERROR)
{
    _ptrTargetStream = new std::ofstream(inFileName.c_str());
    Logger::regSink(*this);
}

Sink::~Sink()
{
    Logger::remSink(*this);
    if(_deleteStreamAtDestroy)
        delete _ptrTargetStream;
    _ptrTargetStream = NULL;
}

/** write a message to the sink
 *  @note This is an internal function.
 *  @param[in] inLogger: logger from which the message is copied.
 */
int Sink::logIt( const Logger *inLogger )
{
    std::string sevDesc;
    sevDesc = Logger::_severityDescriptor[inLogger->getLastLoggedSev()];
    if( inLogger->getLastLoggedSev() >= _logSeverity )
        *_ptrTargetStream << "[" << boost::posix_time::to_iso_extended_string( inLogger->getLogTime() ) << "]" 
                          << sevDesc << "-" << inLogger->getLoggerName() << "-"
                          << inLogger->getLastLoggedMsg() << std::endl;
    return 0;
}
   
}

